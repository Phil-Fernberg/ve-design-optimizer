# A Geospatial Optimizer for VE Design

## Background
In recent years, there has been a notable uptick in the use of immersive technology to conduct psychological studies. Utilizing virtual environments (VE) created in gaming software engines, whether in virtual reality, on panoramic screens, or computer monitors, allows an unprecedented level of control over the conditions and variables of a study, and allows greater consistency across partcipant groups. The team I currently work with has been engaged in precisely this type of research. However, the development of these VEs is very time and labor intensive, especially for teams comprised of students from a variety of disciplines rather than professional game designers. Perhaps more important is the fact that when VEs are fully built out they require an enormous amount of computing power to render and process in real time, which can lead to crashing the system in the middle of an experiment, requiring either a complete restart or nullifying of the data collected in said experiment. 

## Problems
One common strategy for addressing this issue is to build a VE with low poly (or low resolution/detail) materials. This reduces things like trees, buildings, and roads to their simplest forms, adding only minimal details and textures, which significantly cuts the processing speed and memory needed for projecting objects. It also means sacrificing high quality imagery in the environment, taking away a sense of "realness" or embodiment. Another option is to maintain high quality, detailed elements in the virtual world but to only place them in places where a user will actually see them while using either low poly or no assets at all in spaces outside of the participants viewshed. This strategy can also cut down on processing time and power, but it can be difficult to predict the best asset placement if the user is free to move and look around a rather large space (e.g. a neighborhood) and could potentially run into unexpected holes in the VE. How can we get the best of both worlds?

## Using Python and GIS for Virtual World Design Optimization
I believe programming in GIS can help us solve this problem. I propose creating a spatial optimizer which, given the roads, zoning, and participant route as inputs, can automatically add building massings and create a viewshed analysis for the route. This viewshed analysis will help our team determine what areas of a VE the study participant will actually see and therefore need to be fully detailed and build out, and what areas we can either build with less detail or not at all to cut down on labor and processing power needed for the model to run.

## Data and Parameters
The optimizer will need three four ***data*** inputs:
*    Shapefile of centerlines in the designed road network (drawn by our team in GIS or CAD)
*    Shapefile of character districts, color coded by residential, commercial, industrial, and park (drawn by our team as a hexagonal grid in GIS or CAD)
*    Shapefile with polyline geometry of the route participants will take through the world (drawn by our team in GIS or CAD)
*    Shapefile of building footprints geometry (drawn by our team in GIS or CAD)

Each of these layers will also have ***parameters*** attached to them which will be as follows:
*    Centerlines
        * where any centerline crosses character district x, it will be assigned a relevant buffer width of y
        * where each centerline receives its buffer, it will receive an additional buffer of x according to district
*    Character Districts
        * each district x will have a relevant range of possible building heights according to intensity, commercial > industrial > residential > park
*    Route
        * the route will have a certain ratio of districts it is exposed to
*    Building footprints
        * footprints will have a type of building associated with them according to type of district they overlap with
    
## System Design
The flow of the of the optimizer will be as follows:
1. Receive all inputs and verify they are properly geolocated
2. Analyze the road network and add points to every intersection where the road length goes from one district to another
3. Break up the line segments of the centerlines between each transition point so that every length is isolated to a relative district
4. Create road with buffer polygons of each length according to the district it overlaps with
5. Create sidewalk buffer from the results according to relative districts
6. Assign heights and building types to each building footprint according to underlying district
7. Create DEM with massings and road geometry (should hopefully be a closed planar surface with building masses)
8. Use DEM and route shapefile to perform a viewshed analysis and create an isovist for the participant
9. Color code isovist and plot result so we can immediately see where to build or not to build
10. Export as file format that can be brought into Unity gaming engine and overlaid onto our project

### Possible Limitations
*    Because the district map is a hex grid, there may be moments where the roads transition between zones in an awkward part of the polygon which may have funky parameterizing as a result
*    The sidewalk buffering might not bring surfaces flush to building footprints and could possibly create holes in the model as a result

### Solutions
*    Ensure roads have a clear threshold that decides which district they fall into
*    If there are holes in DEM fill them using Void Fill function
