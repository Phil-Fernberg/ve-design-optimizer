# Virtual World Optimizer
In recent years, there has been a notable uptick in the use of immersive technology to conduct psychological studies. Utilizing virtual environments (VE) created in gaming software engines, whether in virtual reality, on panoramic screens, or computer monitors, allows an unprecedented level of control over the conditions and variables of a study, and allows greater consistency across partcipant groups. The team I currently work with has been engaged in precisely this type of research. However, the development of these VEs is very time and labor intensive, especially for teams comprised of students from a variety of disciplines rather than professional game designers. Perhaps more important is the fact that when VEs are fully built out they require an enormous amount of computing power to render and process in real time, which can lead to crashing the system in the middle of an experiment, requiring either a complete restart or nullifying of the data collected in said experiment. I have created a tool which can help solve this problem using a combination of ArcGIS toolboxes and custom python scripts.

## A GIS-based Solution
The Virtual World Optimizer is an ArcGIS toolbox for VE development which can automatically generate a street network, add adjacent building massings, and create a viewshed analysis for a given user route. This viewshed analysis can help determine what areas of a VE a player (or study participant) will actually see and therefore need to be fully detailed and build out, and what areas can be built with less detail or not at all to cut down on labor and processing power needed for the model to run.

## How it works
A user provides two inputs: a polyline shapefile which signifies the route a player takes in a study and a polygon shapefile of zones (such as land uses or architectural character). The zone data are ideally color coded and include what the desired building heights are for each zone as well as desired road widths, each in a field of the attribute table. 

The model then generates points at the edge vertices of each zone, randomly interpolates branching polylines from the route which are then merged with the original polyline. 

The model then creates a buffer using the street widths parameters, erases the respective area from the original zones geometry, and creates massings with the remaining geometry according to height parameters. 

Finally, the buffer and massings are all merged into one raster and put through a viewshed analysis using the route as the viewpoint from ground level.

## The Result
The output is a visiblity analysis visualization which can be exported and built into a terrain in the Unity gaming engine. When this is done, game designers can build directly onto the map according to the color coded analysis, using a high level of detail in highly visible areas, and less or no detail otherwise, saving time, cognitive labor, and processing power.

The Optimizer is still under development, but you are free to download the toolbox, play with the model and build it better if you so desire. We hope it can be useful for anyone doing spatial research in VR!
