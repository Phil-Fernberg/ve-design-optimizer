# Phil Fernberg
# NR 6920
# VE Design Optimizer

import arcpy
import os 

# Set the workspace allow overwrites.
arcpy.env.workspace = r'C:\Users\Phil\Documents\USU Coursework\NR6920\ve-design-optimizer\VEDesignOptimizer'
arcpy.env.overwriteOutput = True

# Create variables to get parameters for each file
infcPoint = arcpy.GetParameterAsText(0)
infcBranch = arcpy.GetParameterAsText(1)
#output_lc = arcpy.GetParameterAsText(2)
output_fn = arcpy.GetParameterAsText(2)

# default spatial reference
srs = arcpy.Describe(infcBranch).spatialReference

#fc = arcpy.management.CreateFeatureclass(out_path=output_lc, out_name=output_fn, geometry_type='POLYLINE',spatial_reference=srs)
fc = arcpy.management.CreateFeatureclass(out_path=os.path.dirname(output_fn), out_name=os.path.basename(output_fn), geometry_type='POLYLINE',spatial_reference=srs)

# Make cursors for each input file
branchPathCursor = arcpy.da.SearchCursor(infcBranch, ["pathPointID", "SHAPE@X", "SHAPE@Y"])
pathPointCursor = arcpy.da.SearchCursor(infcPoint, ["pathPointID", "SHAPE@X", "SHAPE@Y"])
branchCursor = arcpy.da.InsertCursor(fc, ['SHAPE@'])

# Start making the polylines
for branchRow in branchPathCursor:

    # get xy value from branchPath
    # print(str(branchRow[1]) + " " + str(branchRow[2]))
    branchPoint = arcpy.Point(branchRow[1], branchRow[2])

    # get xy value from pathPoint
    for pathRow in pathPointCursor:
        if branchRow[0] == pathRow[0]:
            nearestPathPoint = arcpy.Point(pathRow[1], pathRow[2])  # this is the point in the original pathPoint
            break

    # reset the cursor back to the first row
    pathPointCursor.reset()

    # Write the geometry, here is the source: https://pro.arcgis.com/en/pro-app/latest/arcpy/get-started/writing-geometries.htm 
    # generate the point A (branch) to point B (path)
    array = arcpy.Array([branchPoint, nearestPathPoint])
    polyline = arcpy.Polyline(array, srs)

    # prepare the cursor in the new polyline file
    branchCursor.insertRow([polyline])

del branchPathCursor
del pathPointCursor
del branchCursor
